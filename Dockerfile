# Dockerfile for building your DHT protocol
FROM rust:latest

WORKDIR /app

# Copy your project files to the container
COPY . .

# Build your project
RUN cargo build --release

# Expose ports used by your DHT protocol
EXPOSE 5000/udp
