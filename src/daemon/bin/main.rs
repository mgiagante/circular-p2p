// https://stackoverflow.com/questions/36604010/how-can-i-build-multiple-binaries-with-cargo
//! This is the daemon component of the nodes. It listens for connections to receive messages.
use circular_p2p::network::message::Message;
use circular_p2p::network::{LocalNetwork, NetworkAdapter};
use circular_p2p::node::Node;
use log::info;
use std::{
    env,
    io::BufReader,
    net::{TcpListener, TcpStream},
};

fn main() {
    env_logger::init();
    env::set_var("RUST_LOG", "info");

    let my_port;
    let my_peer_id;
    let mut predecessor_addr: Option<String> = None;
    let mut successor_addr: Option<String> = None;

    let args: Vec<_> = env::args().collect();

    if args[1] == "--bootstrap" {
        my_peer_id = &args[2];
        my_port = &args[3];
    } else {
        my_peer_id = &args[1];
        my_port = &args[2];
        predecessor_addr = Some(args[3].clone());
        successor_addr = Some(args[4].clone());
    }

    let listener = TcpListener::bind(format!("127.0.0.1:{}", &my_port)).unwrap();

    let mut node = Node::new(Some(my_peer_id.clone()), predecessor_addr, successor_addr);

    info!("cp2p daemon listening on port {}", &my_port);

    // We iterate over all connections, continuously being received by the listener.
    for stream in listener.incoming() {
        let stream = stream.unwrap(); // Since we're reading connection attempts, we need to unwrap them to make sure they are not errors.

        handle_connection(stream, &my_peer_id.to_string(), &mut node);
    }
}

fn handle_connection(mut stream: TcpStream, _my_peer_id: &String, node: &mut Node) {
    let buf_reader = BufReader::new(&mut stream);
    let message: Message = LocalNetwork::receive(buf_reader);

    info!("Message Received: {:?}", message);

    // The node should not do networking. It should only
    // return a decision to this code on what to do about the message at a network level.
    let response = node.route(message).unwrap();

    LocalNetwork::respond_with(response, &mut stream)
    // stream is dropped here since this function took ownership of it.
}
