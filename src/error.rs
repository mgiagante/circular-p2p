use thiserror::Error;

#[derive(Error, Debug)]
pub enum CP2PError {
    #[error("Failed to parse message: {context}")]
    MessageParsingError { context: String },

    #[error("Failed to parse address: {context}")]
    AddressParsingError { context: String },

    #[error("Unknown error.")]
    Unknown,
}
