use super::network::message::Message;
use crate::error::CP2PError;
use regex::Regex;
// use std::fmt;
use std::io::{prelude::*, BufReader};
use std::net::TcpStream;

pub mod message;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PeerAddress {
    pub ip: String,
    pub port: String,
    pub peer_id: String,
}

/*
impl fmt::Display for PeerAddress {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}/{}", self.ip, self.port, self.peer_id)
    }
}
*/

impl TryFrom<String> for PeerAddress {
    // Format looks like this: "198.32.2.1:1234/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
    type Error = CP2PError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        let regexp = Regex::new(r"^([\d\.]+):(\d+)/(\w+)$").unwrap();
        let captures = regexp
            .captures(&s)
            .ok_or(Self::Error::AddressParsingError {
                context: format!("Cannot parse PeerAddress {}", s),
            })?;

        let ip = captures.get(1).unwrap().as_str();
        let port = captures.get(2).unwrap().as_str();
        let peer_id = captures.get(3).unwrap().as_str();

        Ok(Self {
            ip: ip.to_owned(),
            port: port.to_owned(),
            peer_id: peer_id.to_owned(),
        })
    }
}

impl From<PeerAddress> for String {
    fn from(peer_addr: PeerAddress) -> String {
        format!("{}:{}/{}", peer_addr.ip, peer_addr.port, peer_addr.peer_id)
    }
}

/// `NetworkAdapter` is a trait to be implemented by different kinds of network mechanisms.
pub trait NetworkAdapter {
    fn send(message: Message) -> Message;
    // fn peer_addr(ip_addr: Option<Ipv4Addr>, peer_id: &str) -> String;

    /// To be used by the daemon to reply to messages from another node's client component.
    /// Very similar to #send() but it does not wait for a response, because it is a response
    /// itself.
    fn respond_with(message: Message, stream: &mut TcpStream) {
        stream.write_all(String::from(message).as_bytes()).unwrap();
        // stream is dropped here since this function took ownership of it.
    }

    /// This is how the daemon or client components of the nodes receive messages from the network.
    /// It turns what is read from the TCP stream into a message object.
    fn receive(buf_reader: BufReader<&mut TcpStream>) -> Message {
        let message_string = buf_reader
            .lines()
            .map(|result| result.unwrap())
            .take_while(|line| !line.is_empty())
            .filter(|line| !line.is_empty())
            // .collect::<Vec<String>>();
            .collect::<String>();

        // TODO: Try to replace this by Message::from(message_string).
        // For that I need to implement Message::From<String>. That will include identifying the
        // body by using the kind and seeing if it has the right content.
        // The message used will probably a try_* version since the operation is fallible.
        Message::try_from(message_string).unwrap()
    }
}

/// `LocalNetwork` is for using the p2p network inside your local network.
/// All nodes will be assumed to be in localhost and are distinguished by their port number.
pub struct LocalNetwork;

impl NetworkAdapter for LocalNetwork {
    /// This is how the client component of the nodes sends messages to the network.
    /// Then it waits for a response and returns it.
    fn send(message: Message) -> Message {
        let peer_addr: PeerAddress = message.receiver.clone().into();
        let mut stream = TcpStream::connect(format!("{}:{}", peer_addr.ip, peer_addr.port))
            .expect("Could not connect to peer.");

        let message_string: String = message.into();
        stream.write_all(message_string.as_bytes()).unwrap();

        // Read from stream until the whole response message is received. Then parse it into a message
        // object and return it.
        let buf_reader = BufReader::new(&mut stream);
        Self::receive(buf_reader)
        // stream is dropped here since this function took ownership of it.
    }
}

/// `Internet` is for using the p2p network on the internet.
/// Nodes have their own IP address and listen on the same port number.
pub struct Internet;

impl NetworkAdapter for Internet {
    /// This is how the client component of the nodes sends messages to the network.
    /// Then it waits for a response and returns it.
    fn send(message: Message) -> Message {
        // TODO: Find a way to obtain the IP of the node and send it in the call to
        // Self::peer_addr() intead of None.
        let peer_addr: PeerAddress = message.receiver.clone().into();

        let mut stream = TcpStream::connect(format!("{}:{}", peer_addr.ip, peer_addr.port))
            .expect("Could not connect to peer.");

        let message_string: String = message.into();
        stream.write_all(message_string.as_bytes()).unwrap();

        stream.write_all(message_string.as_bytes()).unwrap();

        // Read from stream until the whole response message is received. Then parse it into a message
        // object and return it.
        let buf_reader = BufReader::new(&mut stream);
        Self::receive(buf_reader)
        // stream is dropped here since this function took ownership of it.
    }

    /*
    fn peer_addr(ip_addr: Option<Ipv4Addr>, peer_id: &str) -> String {
        format!("{}:{}", ip_addr.unwrap(), peer_id)
    }
    */
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_build_peer_address_from_string() {}

    #[test]
    fn should_convert_peer_address_into_string() {}
}
