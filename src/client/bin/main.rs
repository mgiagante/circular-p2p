//! The client can send messages to other nodes.
use circular_p2p::network::message::Message;
use circular_p2p::network::{LocalNetwork, NetworkAdapter};
use log::info;
use std::env;

fn main() {
    env_logger::init();
    env::set_var("RUST_LOG", "info");

    let message_string = env::args().skip(1).collect::<Vec<String>>().join("\n");

    let message = Message::try_from(message_string).expect("Malformed message!");

    LocalNetwork::send(message);
    info!("Message sent!");
    // TODO: Find out how to get the peer_id for this node. Maybe read it from an env var
    // set by the daemon.
    // Client always sends messages to its own daemon.
    // Then its daemon will forward them if needed or resolve them by itself.
    /* Messages the client can issue:
            SET "City" => "London"
            GET "City"
            LEAVE
    */
}
