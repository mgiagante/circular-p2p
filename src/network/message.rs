use super::PeerAddress;
use crate::error::CP2PError;
use regex::Regex;
use std::convert::TryFrom;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MessageKind {
    Set,
    Get,
    SetPred,
    SetSucc,
    Join,
    Ping,
    Leave,
    Failure,
    Success,
}

impl From<MessageKind> for String {
    fn from(message_kind: MessageKind) -> Self {
        match message_kind {
            MessageKind::Set => format!("SET"),
            MessageKind::Get => format!("GET"),
            MessageKind::SetPred => format!("SET_PRED"),
            MessageKind::SetSucc => format!("SET_SUCC"),
            MessageKind::Join => format!("JOIN"),
            MessageKind::Ping => format!("PING"),
            MessageKind::Leave => format!("LEAVE"),
            MessageKind::Success => format!("SUCCESS"),
            MessageKind::Failure => format!("FAILURE"),
        }
    }
}

impl TryFrom<String> for MessageKind {
    type Error = CP2PError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        match s.as_str() {
            "SET" => Ok(Self::Set),
            "GET" => Ok(Self::Get),
            "SET_PRED" => Ok(Self::SetPred),
            "SET_SUCC" => Ok(Self::SetSucc),
            "JOIN" => Ok(Self::Join),
            "PING" => Ok(Self::Ping),
            "LEAVE" => Ok(Self::Leave),
            "FAILURE" => Ok(Self::Failure),
            "SUCCESS" => Ok(Self::Success),
            _ => Err(Self::Error::MessageParsingError {
                context: "Invalid kind during MessageKind parsing.".to_owned(),
            }),
        }
    }
}

// Using Debug here since MessageKind variants do not hold any data.
/*
impl fmt::Display for MessageKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
*/

#[derive(Debug, Eq, PartialEq)]
pub enum MessageBody {
    Set { key: String, value: String }, // key and value
    Get { key: String },                // key
    SetPred(PeerAddress),               // address to be set as predecessor of the receiver
    SetSucc(PeerAddress),               // address to be set as successor of the receiver
    Join(PeerAddress),                  // address of the joining node
    Ping(String),                       // text description of why node is being pinged.
    Success(String),                    // text description of what operation was successful
    Failure(String),                    // text description of what operation went wrong
    Leave,                              // just telling the network this node is leaving.
}

impl MessageBody {
    pub fn key(&self) -> Option<String> {
        match self {
            Self::Set { key, value: _ } => Some(key.to_owned()),
            Self::Get { key } => Some(key.to_owned()),
            _ => None,
        }
    }

    pub fn value(&self) -> Option<String> {
        match self {
            Self::Set { key, value: _ } => Some(key.to_owned()),
            _ => None,
        }
    }

    pub fn peer_addr(&self) -> Option<PeerAddress> {
        // Using if let to avoid the duplication a match statement would cause.
        if let Self::SetPred(peer_addr) | Self::SetSucc(peer_addr) | Self::Join(peer_addr) = self {
            Some(peer_addr.to_owned())
        } else {
            None
        }
    }
}

impl From<MessageBody> for String {
    fn from(body: MessageBody) -> String {
        match body {
            MessageBody::Set { key, value } => format!("{} => {}", key, value),
            MessageBody::Get { key } => format!("{}", key),
            MessageBody::SetPred(peer_addr) => format!("{}", String::from(peer_addr)),
            MessageBody::SetSucc(peer_addr) => format!("{}", String::from(peer_addr)),
            MessageBody::Join(peer_addr) => format!("{}", String::from(peer_addr)),
            MessageBody::Ping(description) => format!("{}", description),
            MessageBody::Success(description) => format!("{}", description),
            MessageBody::Failure(description) => format!("{}", description),
            MessageBody::Leave => format!(""),
        }
    }
}

impl TryFrom<String> for MessageBody {
    type Error = CP2PError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        // These regexes catch the body so it can be delegated to the corresponding try_into()
        // parsing method if the case requires it.
        let set_regex = Regex::new(r"SET \|\s*([\w\s!?\-]+)\s*\| => \|\s*([\w\s!?\-]+)\s*\|");
        let get_regex = regex::Regex::new(r"GET \|\s*([\w\s!?\-]+)\s*\|");
        let join_regex = Regex::new(r"JOIN (.+)");
        let leave_regex = Regex::new(r"LEAVE ");
        let set_pred_regex = Regex::new(r"SET_PRED (.+)");
        let set_succ_regex = Regex::new(r"SET_SUCC (.+)");
        let ping_regex = Regex::new(r"PING (.+)");
        let success_regex = Regex::new(r"SUCCESS (.+)");
        let failure_regex = Regex::new(r"FAILURE (.+)");

        if let Some(captures) = set_regex.unwrap().captures(&s) {
            let key = captures.get(1).unwrap().as_str().to_owned();
            let value = captures.get(2).unwrap().as_str().to_owned();
            Ok(MessageBody::Set { key, value })
        } else if let Some(captures) = get_regex.unwrap().captures(&s) {
            let key = captures.get(1).unwrap().as_str().to_owned();
            Ok(MessageBody::Get { key })
        } else if let Some(captures) = join_regex.unwrap().captures(&s) {
            // TODO: See if this can be shortened...
            let peer_address: PeerAddress = captures
                .get(1)
                .unwrap()
                .as_str()
                .to_owned()
                .try_into()
                .unwrap();
            Ok(MessageBody::Join(peer_address))
        } else if leave_regex.unwrap().captures(&s).is_some() {
            Ok(MessageBody::Leave)
        } else if let Some(captures) = ping_regex.unwrap().captures(&s) {
            let description: String = captures.get(1).unwrap().as_str().to_owned();
            Ok(MessageBody::Ping(description))
        } else if let Some(captures) = set_pred_regex.unwrap().captures(&s) {
            // TODO: See if this can be shortened...
            let peer_address: PeerAddress = captures
                .get(1)
                .unwrap()
                .as_str()
                .to_owned()
                .try_into()
                .unwrap();
            Ok(MessageBody::SetPred(peer_address))
        } else if let Some(captures) = success_regex.unwrap().captures(&s) {
            let description: String = captures.get(1).unwrap().as_str().to_owned();
            Ok(MessageBody::Success(description))
        } else if let Some(captures) = failure_regex.unwrap().captures(&s) {
            let description: String = captures.get(1).unwrap().as_str().to_owned();
            Ok(MessageBody::Failure(description))
        } else if let Some(captures) = set_succ_regex.unwrap().captures(&s) {
            // TODO: See if this can be shortened...
            let peer_address: PeerAddress = captures
                .get(1)
                .unwrap()
                .as_str()
                .to_owned()
                .try_into()
                .unwrap();
            Ok(MessageBody::SetSucc(peer_address))
        } else {
            Err(Self::Error::MessageParsingError {
                context: format!("Invalid body during MessageBody parsing. {:?}", s),
            })
        }
    }
}

/*
impl fmt::Display for MessageBody {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Set { key, value } => write!(f, "SET {} => {}", key, value),
            Self::Get { key } => write!(f, "GET {}", key),
            Self::SetPred(peer_addr) => write!(f, "SET_PRED {}", peer_addr),
            Self::SetSucc(peer_addr) => write!(f, "SET_SUCC {}", peer_addr),
            Self::Join(peer_addr) => write!(f, "JOIN {}", peer_addr),
            Self::Ping(description) => write!(f, "PING {}", description),
            Self::Success(description) => write!(f, "SUCCESS {}", description),
            Self::Failure(description) => write!(f, "FAILURE {}", description),
            Self::Leave => write!(f, "LEAVE"),
        }
    }
}
*/

#[derive(Debug, Eq, PartialEq)]
pub struct Message {
    pub kind: MessageKind,
    pub body: MessageBody,
    pub sender: PeerAddress,
    pub receiver: PeerAddress,
}

impl From<Message> for String {
    fn from(message: Message) -> Self {
        format!(
            "{}\r\n{}\r\n{}\r\n{}\r\n",
            String::from(message.kind),
            String::from(message.body),
            String::from(message.sender),
            String::from(message.receiver)
        )
    }
}

impl TryFrom<String> for Message {
    type Error = CP2PError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        // SET
        // City => London
        // 0.0.0.1:7001/fc8254ae0ec6c028e9d66db5a794b208fcb5f2840c3a8e94546e38841135db4a
        // 0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696
        // [new line] [carriage return]

        let fields: Vec<&str> = s.lines().collect();

        let kind: MessageKind = fields[0].to_owned().try_into()?;

        // Test [KIND] + [BODY] to be able to infer what the body means in the context of that kind.
        let body: MessageBody = format!("{} {}", fields[0], fields[1]).try_into()?;

        let sender: PeerAddress = fields[2].to_owned().try_into()?;
        let receiver: PeerAddress = fields[3].to_owned().try_into()?;

        Ok(Message {
            kind,
            body,
            sender,
            receiver,
        })
    }
}

/*
impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // should be able to replace all this by &self.into()
        write!(
            f,
            "{}\r\n{}\r\n{}\r\n{}\r\n\r\n",
            self.kind, self.body, self.sender, self.receiver
        )
    }
}
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_build_set_message_from_string() {
        let kind = "SET".to_owned();
        let body = "|Favorite city| => |London and Manchester|".to_owned();
        let sender =
            "0.0.0.1:7001/fc8254ae0ec6c028e9d66db5a794b208fcb5f2840c3a8e94546e38841135db4a"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Set,
            body: MessageBody::Set {
                key: "Favorite city".to_owned(),
                value: "London and Manchester".to_owned(),
            },
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "fc8254ae0ec6c028e9d66db5a794b208fcb5f2840c3a8e94546e38841135db4a"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_get_message_from_string() {
        let kind = "GET".to_owned();
        let body = "|Favorite city|".to_owned();
        let sender =
            "0.0.0.1:7001/fc8254ae0ec6c028e9d66db5a794b208fcb5f2840c3a8e94546e38841135db4a"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Get,
            body: MessageBody::Get {
                key: "Favorite city".to_owned(),
            },
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "fc8254ae0ec6c028e9d66db5a794b208fcb5f2840c3a8e94546e38841135db4a"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_join_message_from_string() {
        let kind = "JOIN".to_owned();
        let body = "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
            .to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Join,
            body: MessageBody::Join(PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            }),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_set_pred_message_from_string() {
        // Set me as your PRED
        let kind = "SET_PRED".to_owned();
        let body = "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
            .to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::SetPred,
            body: MessageBody::SetPred(PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            }),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_set_succ_message_from_string() {
        // Set me as your SUCC
        let kind = "SET_SUCC".to_owned();
        let body = "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
            .to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::SetSucc,
            body: MessageBody::SetSucc(PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            }),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_leave_message_from_string() {
        let kind = "LEAVE".to_owned();
        let body = "".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Leave,
            body: MessageBody::Leave,
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_ping_message_from_string() {
        let kind = "PING".to_owned();
        let body = "Are you alive?".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Ping,
            body: MessageBody::Ping("Are you alive?".to_owned()),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_success_message_from_string() {
        let kind = "SUCCESS".to_owned();
        let body = "Key was stored successfully.".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Success,
            body: MessageBody::Success("Key was stored successfully.".to_owned()),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_failure_message_from_string() {
        let kind = "FAILURE".to_owned();
        let body =
            "Key was not stored because the node responsible for it could not be found.".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let expected_message = Message {
            kind: MessageKind::Failure,
            body: MessageBody::Failure(
                "Key was not stored because the node responsible for it could not be found."
                    .to_owned(),
            ),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        assert_eq!(Message::try_from(message_string).unwrap(), expected_message)
    }

    #[test]
    fn should_build_message_from_string_and_convert_back_to_string() {
        let kind = "FAILURE".to_owned();
        let body =
            "Key was not stored because the node responsible for it could not be found.".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        assert_eq!(
            String::from(Message::try_from(message_string.clone()).unwrap()),
            message_string
        )
    }

    #[test]
    fn should_convert_message_into_string() {
        let message = Message {
            kind: MessageKind::Failure,
            body: MessageBody::Failure(
                "Key was not stored because the node responsible for it could not be found."
                    .to_owned(),
            ),
            sender: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7001".to_owned(),
                peer_id: "e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                    .to_owned(),
            },
            receiver: PeerAddress {
                ip: "0.0.0.1".to_owned(),
                port: "7002".to_owned(),
                peer_id: "eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                    .to_owned(),
            },
        };

        let kind = "FAILURE".to_owned();
        let body =
            "Key was not stored because the node responsible for it could not be found.".to_owned();
        let sender =
            "0.0.0.1:7001/e14a747f150b845a183569bbfc821b5a9f64f6a2434487fe2b79419162884cb6"
                .to_owned();
        let receiver =
            "0.0.0.1:7002/eaa93cb9a2efe3c9cafca6807477a79e35f074e1a0d414b599458b63ea520696"
                .to_owned();

        let message_string = format!("{}\r\n{}\r\n{}\r\n{}\r\n", kind, body, sender, receiver);

        let string_from_message: String = message.into();
        assert_eq!(string_from_message, message_string)
    }
}
