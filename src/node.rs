use super::network::message::{Message, MessageKind};
use super::network::PeerAddress;
use log::info;
use rand::{thread_rng, Rng};
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

fn sha2_hash(input: &str) -> String {
    let mut hasher = DefaultHasher::new();
    input.hash(&mut hasher);
    format!("{:016x}", hasher.finish())
}

pub struct Node {
    peer_id: String,
    predecessor_addr: Option<PeerAddress>,
    successor_addr: Option<PeerAddress>,
    data: HashMap<String, String>,
}

impl Node {
    pub fn new(
        peer_id: Option<String>,
        predecessor_addr: Option<String>,
        successor_addr: Option<String>,
    ) -> Self {
        Self {
            // Use the provided peer_id. If not present, generate a random SHA2 as the peer_id.
            peer_id: peer_id.unwrap_or_else(|| {
                // Generate a random byte array of length 32
                let mut rng = thread_rng();
                let bytes: Vec<u8> = (0..32).map(|_| rng.gen_range(0..=255)).collect();

                // Convert the byte array to a String
                let random_string = String::from_utf8(bytes).unwrap();

                // Return the SHA2 hash of that string
                sha2_hash(random_string.as_str())
            }),
            predecessor_addr: Some(predecessor_addr.unwrap().try_into().unwrap()), // TODO: Modify this to
            // handle when node is
            // --bootstrap and has None
            // here.
            successor_addr: Some(successor_addr.unwrap().try_into().unwrap()),
            data: HashMap::new(),
        }
    }

    // TODO: Unit test methods called by this, starting with the SET and GET paths.
    pub fn route(&mut self, request: Message) -> Option<Message> {
        match &request.kind {
            MessageKind::Set => self.handle_set(request),
            MessageKind::Get => self.handle_get(request),
            MessageKind::SetPred => self.handle_set_pred(request),
            MessageKind::SetSucc => self.handle_set_succ(request),
            MessageKind::Join => self.handle_join(request),
            MessageKind::Ping => self.handle_ping(request),
            MessageKind::Failure => self.handle_failure(request),
            MessageKind::Success => self.handle_success(request),
            MessageKind::Leave => self.handle_ping(request),
        }
    }

    fn handle_set(&mut self, request: Message) -> Option<Message> {
        if self.is_responsible_for(request.body.key().unwrap()) {
            self.data
                .insert(request.body.key().unwrap(), request.body.value().unwrap());
            // Send a SUCCESS message to the originator.
            todo!()
        } else {
            // TODO: If it is responsible for the data, store it in `data`.
            // Then send a success message to the
            // originator of the SET call, which is a client component.
            // If not, ask this node's successor to store the data.
            // This means the message nodes will pass on to each other needs to include the
            // peer_addr of the originator so the storing node can talk back to it.
            // The process is async.
            todo!();
        }
    }

    fn handle_get(&self, request: Message) -> Option<Message> {
        // TODO: Work with &request.body and use responsible_for() to find out if this node
        // is supposed to have the data in its piece of the DHT.
        // If it is responsible for the data, retrieve it from `data` and respond with SUCCESS
        // and the value.
        // If not, ask this node's successor.
        todo!();
    }

    fn handle_set_pred(&self, request: Message) -> Option<Message> {
        todo!()
    }

    fn handle_set_succ(&self, request: Message) -> Option<Message> {
        todo!()
    }

    fn handle_join(&self, request: Message) -> Option<Message> {
        todo!()
    }

    fn handle_failure(&self, request: Message) -> Option<Message> {
        todo!()
    }

    fn handle_success(&self, request: Message) -> Option<Message> {
        todo!()
    }

    fn handle_ping(&self, request: Message) -> Option<Message> {
        todo!()
        /*
        return Some(Message {
            kind: "PONG".to_string(),
            body: "Received!".to_string(),
            receiver: request.sender,
            sender: self.peer_id.to_string(),
        });
        */
    }

    fn handle_error(&self, request: Message) -> Option<Message> {
        todo!()
        /*
        return Some(Message {
            kind: "ERROR".to_string(),
            body: "ERR1 - Invalid request.".to_string(),
            receiver: request.sender,
            sender: self.peer_id.to_string(),
        });
        */
    }

    pub fn predecessor_addr(&self) -> PeerAddress {
        self.predecessor_addr.clone().unwrap()
    }

    pub fn is_responsible_for(&self, key: String) -> bool {
        let key_hash = sha2_hash(key.as_str());
        //panic!("is {} responsible for {}", &self.peer_id, key_hash);

        info!("is {} responsible for {}", &self.peer_id, key_hash);
        // If I'm greater than the key, I might be the immediate successor...
        if self.peer_id > key_hash {
            // If my predecessor is lesser than or equal to the key, then I am!
            if self.predecessor_addr().peer_id <= key_hash {
                true
            // Otherwise, the immediate successor might be either farther away back in the circle,
            // or forward.
            } else {
                false
            }
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    // peer_id: 3d101a9ee87a2722
    // predecessor: 0
    // successor: 3d101a9ee87a2921
    // key: 177 - 3d101a9ee87a2721
    fn should_be_responsible_for_key_lesser_than_its_peer_id() {
        let node = Node::new(
            Some("3d101a9ee87a2722".to_owned()),
            Some("0.0.0.0:1000/0".to_owned()),
            Some("0.0.0.1:1000/3d101a9ee87a2921".to_owned()),
        );

        assert_eq!(node.is_responsible_for("City".to_string()), true)
    }

    #[test]
    // peer_id: 200 - 3d101a9ee87a2742
    // predecessor: 179 - 3d101a9ee87a272d
    // successor: 210 - 3d101a9ee87a274c
    // key: 177 - 3d101a9ee87a2721
    fn should_defer_if_predecessor_is_closer_to_key_than_itself() {
        let node = Node::new(
            Some("3d101a9ee87a2742".to_owned()),
            Some("0.0.0.0:1000/3d101a9ee87a272d".to_owned()),
            Some("0.0.0.1:1000/3d101a9ee87a274c".to_owned()),
        );

        assert_eq!(node.is_responsible_for("City".to_string()), false)
    }

    #[test]
    // peer_id: 175 - 3d101a9ee87a271f
    // predecessor: 100 - 3d101a9ee87a26d4
    // successor: 0
    // key: 177 - 3d101a9ee87a2721
    fn should_defer_to_keys_immediate_successor_by_wrapping_around() {
        let node = Node::new(
            Some("3d101a9ee87a271f".to_owned()),
            Some("0.0.0.0:1000/3d101a9ee87a26d4".to_owned()),
            Some("0.0.0.1:1000/0".to_owned()),
        );

        assert_eq!(node.is_responsible_for("City".to_string()), false)
    }

    #[test]
    // peer_id: 2 - 3d101a9ee87a2672
    // predecessor: 1 - 3d101a9ee87a2671
    // successor: 200 - 3d101a9ee87a2742
    // key: 177 - 3d101a9ee87a2721
    fn should_not_be_responsible_for_key_greater_than_its_peer_id() {
        let node = Node::new(
            Some("3d101a9ee87a2672".to_owned()),
            Some("0.0.0.0:1000/3d101a9ee87a2671".to_owned()),
            Some("0.0.0.1:1000/3d101a9ee87a2742".to_owned()),
        );

        // key_as_peer_id for "City" is 177
        assert_eq!(node.is_responsible_for("City".to_string()), false)
    }

    #[test]
    // peer_id: 177 - 3d101a9ee87a2721
    // predecessor: 15 - 3d101a9ee87a267f
    // successor: 200
    // key: 177 - 3d101a9ee87a2721
    fn should_not_be_responsible_for_key_equal_to_its_peer_id() {
        let node = Node::new(
            Some("3d101a9ee87a2721".to_owned()),
            Some("0.0.0.0:1000/3d101a9ee87a267f".to_owned()),
            Some("0.0.0.1:1000/3d101a9ee87a2742".to_owned()),
        );

        // key_as_peer_id for "City" is 177
        assert_eq!(node.is_responsible_for("City".to_string()), false)
    }

    #[test]
    fn should_make_node_address_from_string() {
        let addr: PeerAddress = "0.0.0.0:1000/3d101a9ee87a2742"
            .to_owned()
            .try_into()
            .unwrap();

        assert_eq!(addr.ip, "0.0.0.0".to_owned());
        assert_eq!(addr.port, "1000".to_owned());
        assert_eq!(addr.peer_id, "3d101a9ee87a2742".to_owned());
    }

    #[test]
    fn should_compute_sha2_hash_of_key() {
        assert_eq!(sha2_hash("City"), "3d101a9ee87a2721".to_owned())
    }
}
