//! circular_p2p is an implementation of a circular p2p network.
pub mod error;
pub mod network;
pub mod node;
