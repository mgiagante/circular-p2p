# Circular p2p
(Video about Circular P2P Networks)[https://yewtu.be/watch?v=-UU_ugiPZ9k]
Peer ID is a SHA2 hash generated randomly as the node joins the DHT.

In a Circular P2P Network, nodes are linked into a circle ordered by peer_id.
That circle wraps around (last node is linked to first node to close the circle).

Each peer is only aware of its immediate predecessor and its immediate successor.
For example, if our network has peers with these IDs:

2, 5, 12, 18, 19

peer with peer_id 12 is only aware of (and can only communicate with) peers 5 and 18.
In order to communicate with them, it needs to know their `PeerAddress` which
contains its IP address, port number and peer ID:
```
"1.1.1.1:7000/34"
```

## How data is stored
Data is stored in-memory only.

## Joining the network
Every time a new node joins the network it generates a random SHA2 hash as its
peer_id. Then it sends the first node (which needs to be publicly known as a bootstrap node)
a "JOIN" message including its `PeerAddress` (IP + port + peer_id).
Then that node will forward the JOIN message through the circle until the
immediate successor of the joining node's peer_id receives it. Let that node be
node B.

1. Node B will tell the joining node to set them as their successor and to
   set its own predecessor (let's call it A) as their predecessor. Joining node
   is all set.
2. And then will tell A to set the joining node as its successor. A is all set.
3. And finally itself will set the joining node as their own predecessor, closing the 
   circle with the new node in it.
4. Node B asks the joining node to store all the data
   that now corresponds to it (any keys for which the joining node is the immediate
   successor).
4. B will send the joining node a SUCCESS message indicating the process
   finished successfully.

Before JOIN process:
A --- B

After JOIN process:
A --- Joining Node --- B
A succ: Joining Node
Joining Node pred: A
Joining Node Succ: B
B pred: Joining Node

## How to insert a key-value pair into the DHT
Suppose we want to insert the pair ("city", "London").

For this we send the following message to our node:
message_kind: "SET"
message_body: "city, London"

When our node receives the SET message, it will hash the key "city" with a hash function. The result we obtain will be
used to decide whether or not this node is responsible of storing the pair.
For this, the hash must always be inside the space of possible peer IDs. The peer_id and the hash must look alike.

How does it decide if it is responsible for storing the pair? By asking if your own peer_id is immediate successor of 
the hash (as if the hash were a peer_id of another node).

If it is not, it will forward the message to its successor.
If it is, it will store it and send a confirmation message to the node that originated the request (unless it itself is the originator).

Requests are async. That means nodes do not wait for a response after sending or
forwarding a SET request. They will receive a confirmation eventually if the
data gets stored by some node.

## How to retrieve a value from the DHT
Suppose we want to retrieve the value for the key "city".

For this we send the following message from our node to the successor:
message_kind: "GET"
message_body: "city"

When that node receives the GET message, it will first see if he himself has it. If not, it will hash
the key "city" with a hash function. The result we obtain will be used to decide whether or not this 
node is responsible of storing the pair.

If it is not, it will forward the message to its successor.
If it is, it will retrieve it from its own portion of the DHT and respond to the node that originated the request
with a "OK" message containing the value.

## Leaving the network
When a node wants to leave the network, it will start a process called
"Graceful Departure".
The leaving node re-inserts all of its data, but this process will exclude
it as a possible candidate to store each key.
Once all its data has been stored in other nodes, it tells its predecessor and
successor to link each other together and shuts down.

If the leaving node has no predecessor or successor (that is, it's the last node in the network),
it will just shut down and data will be lost.

## Peer Churn
- What happens when a node leaves the network?
  If the node shuts down in an orderly way, it should perform the gracefull departure
  process described above.
  If the node crashes, eventually its adjacent nodes will find out its heartbeat is missing
  and link each other together.
- If that node was the only responsible for storing that data, the data becomes
  unavailable.

## To start the server component
```shell
RUST_LOG=info cargo run --bin cp2pd [peer_id] [port_number] [predecessor_addr] [successor_addr]
```

## To use the client to send a message to another node's server component
```shell
RUST_LOG=info cargo run --bin cp2p [message_kind] [message_body] [sender_peer_id] [receiver_peer_id]
```

## To bootstrap a network
```shell
docker compose up
```
and then to stop all nodes run
```shell
docker compose down
```

When bootstraping the network, peer IDs will be randomly generated SHA1 hashes. Then those hashes will be
sorted from lesser to greater to decide which node will be which node's predecessor and successor.

## To-Do's
* Change from TCP to UDP.

## Roadmap
- Implement node shortcuts. Each node will know its predecessor, its successor
  and one shortcut node.
- Make it work in the internet. NAT traversal will be necessary for this.
- Add asymmetric cryptography key-pairs to prevent a node from impersonating
  others.
- Deal with peer churn.

## Blockers
* Implement network joining mechanism with SET_SUCC and SET_PRED messages.
Each node needs to know its predecessor and successor.
  1. That means node 0 has none.
  2. Then node 1 joins, and needs to tell node 0 that he will be his
     predecessor and successor, and set node 0 as his own predecessor and
     successor.
  3. Then node 2 joins, and needs to tell node 0 that he will be
     his predecessor and the node 1 the he will be his successor.
  4. Then node 3 joins, and needs to tell node 0 that he will be
     his predecessor and the node 2 the he will be his successor.

* Implement Node#is_responsible_for() to either return true for a key if his
  successor is farther than the key by himself, or false otherwise.
