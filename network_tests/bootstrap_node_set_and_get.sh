# In this test, the bootstrap node sets a key-value pair and gets it,
# while disconnected from any other nodes.

# Starts the network
docker-compose up

# Joins the network as its first node.
# This sets the $OWN_PEER_ADDRESS environment variable.
docker container exec node_1 sh -c "RUST_LOG=info cargo run --bin cp2p -- --bootstrap JOIN $(hostname -i)"

# Sends itself the SET message to store the "City, London" key-value pair.
# TODO: Make the sender peer address be always implicitly sent as $OWN_PEER_ADDRESS so that the command only needs the receiver's peer address.
docker container exec node_1 sh -c "RUST_LOG=info cargo run --bin cp2p SET "City, London" $OWN_PEER_ADDRESS $OWN_PEER_ADDRESS"

# Sends itself the GET message to retrieve the value of the "City" key.
result = docker container exec node_1 sh -c "RUST_LOG=info cargo run --bin cp2p GET "City" $OWN_PEER_ADDRESS $OWN_PEER_ADDRESS"

# Tears down the network.
docker-compose down

if [[ "$result" == "London" ]]; then
    exit 0
else
    exit 1
fi
